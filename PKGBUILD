# Maintainer: Future Linux Team <future_linux@163.com>

pkgname=cracklib
pkgver=2.9.7
pkgrel=1
pkgdesc="Password Checking Library"
arch=('x86_64')
url="https://github.com/cracklib/cracklib"
license=('GPL')
groups=('base')
depends=('glibc' 'zlib')
makedepends=('bash' 'binutils' 'coreutils' 'gcc' 'grep' 'make')
backup=(usr/share/dict/cracklib-extra-words)
install=${pkgname}.install
source=(https://github.com/cracklib/cracklib/releases/download/v${pkgver}/${pkgname}-${pkgver}.tar.bz2
    https://github.com/cracklib/cracklib/releases/download/v${pkgver}/${pkgname}-words-${pkgver}.bz2)
noextract=(${pkgname}-words-${pkgver}.bz2)
sha256sums=(fe82098509e4d60377b998662facf058dc405864a8947956718857dbb4bc35e6
    ec25ac4a474588c58d901715512d8902b276542b27b8dd197e9c2ad373739ec4)

prepare() {
    cd ${pkgname}-${pkgver}

    sed -i '/skipping/d' util/packer.c

    sed -i '15209 s/.*/am_cv_python_version=3.10/' configure
}

build() {
    cd ${pkgname}-${pkgver}

    PYTHON=python3 CPPFLAGS=-I/usr/include/python3.10
    ${configure}          \
        --disable-static  \
        --with-default-dict=/usr/lib64/cracklib/pw_dict

    make
}

package() {
    cd ${pkgname}-${pkgver}

    make DESTDIR=${pkgdir} install

    install -vDm644 ${srcdir}/${pkgname}-words-${pkgver}.bz2 \
        ${pkgdir}/usr/share/dict/cracklib-words.bz2

    bunzip2 -v               ${pkgdir}/usr/share/dict/cracklib-words.bz2
    ln -v -sf cracklib-words ${pkgdir}/usr/share/dict/words
    echo futurelinux >>      ${pkgdir}/usr/share/dict/cracklib-extra-words
    install -v -m755 -d      ${pkgdir}/usr/lib64/cracklib
}
